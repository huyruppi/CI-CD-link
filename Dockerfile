FROM registry.gitlab.com/huyruppi/cicd-build-node:base

COPY --chown=nginx:nginx ./build/ /usr/share/nginx/html

EXPOSE 80
CMD ["nginx","-g","daemon off;"]
